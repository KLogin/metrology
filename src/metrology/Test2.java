/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metrology;


import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Кирилл
 */
public class Test2 extends javax.swing.JFrame {
    private long time,start,timeConsumedMillis;
    
    public void draw() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    
        setResizable(false);
        setUndecorated(true);
        
        getContentPane().setBackground( Color.WHITE );
        
        Dimension dim = new Dimension( 500, 500 );
        setSize( dim );
        
        setLocationRelativeTo(null);
        
        Oval o = new Oval();
        add(o);
        
        setVisible(true);
        
        double X, Y, R = 140, a=1;
        
        while( R > 0 ) {
            X = 250 + R * Math.cos(a);
            Y = 250 + R * Math.sin(a);
            try {
                o.moveOval(X, Y);
            } catch (InterruptedException ex) {
                Logger.getLogger(Test2.class.getName()).log(Level.SEVERE, null, ex);
            }
            a+=0.001;
            R-=0.0001;
        }
        
        setTime(timeConsumedMillis);
        
        dispose();
    }
    
    private void setTime(long newTime) {
        time = newTime;
    }
    
    public long getTime() {
        return time;
    }
    
    public class Oval extends JComponent {
        Ellipse2D.Double ell = new Ellipse2D.Double();
        double X, Y;
        
        @Override
        public void paint(Graphics g) {
            Graphics2D g2d = (Graphics2D)g;
            g2d.setColor( Color.BLUE );
            ell.setFrame(X, Y, 50, 50);
            start = System.currentTimeMillis();
            g2d.fill(ell);
            g2d.draw(ell);
            timeConsumedMillis += System.currentTimeMillis() - start;
        }
        
        public void moveOval(double newX, double newY) throws InterruptedException {
            X = newX - 25;
            Y = (-1)*newY + 475;
            start = System.currentTimeMillis();
            repaint();
            timeConsumedMillis += System.currentTimeMillis() - start;
        }
    }
}
