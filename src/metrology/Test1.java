/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metrology;

/**
 *
 * @author User
 */
public class Test1 {
    private long time,start,timeConsumedMillis;
    
    private int n = 9999999;
    private double[] x = new double[n+1];
    private double[] y1 = new double[n+1];
    private double[] y2 = new double[n+1];
    private double k1, k2, k3, k4, m1, m2, m3, m4, h, l = 10;
    int i;
    
    public Test1() {
        x[0] = 0;
        y1[0] = 2;
        y2[0] = 3;
    }
    
    public void calculation() {
        start = System.currentTimeMillis();
	h = l/n;
        timeConsumedMillis += System.currentTimeMillis() - start;
	
	for(i=0; i<n; i++)
	{
            start = System.currentTimeMillis();
		x[i] = x[0]+h*i;
		
		k1 = f1( x[i], y1[i], y2[i] );
		m1 = f2( x[i], y1[i], y2[i] );
		k2 = f1( x[i]+h/2, y1[i]+k1*h/2, y2[i]+m1*h/2 );
		m2 = f2( x[i]+h/2, y1[i]+k1*h/2, y2[i]+m1*h/2 );
		k3 = f1( x[i]+h/2, y1[i]+k2*h/2, y2[i]+m2*h/2 );
		m3 = f2( x[i]+h/2, y1[i]+k2*h/2, y2[i]+m2*h/2 );
		k4 = f1( x[i]+h, y1[i]+k3*h, y2[i]+m3*h );
		m4 = f2( x[i]+h, y1[i]+k3*h, y2[i]+m3*h );
		
		y1[i+1] = y1[i] + (h/6)*(k1 + 2*k2 + 2*k3 + k4);
		y2[i+1] = y2[i] + (h/6)*(m1 + 2*m2 + 2*m3 + m4);
            timeConsumedMillis += System.currentTimeMillis() - start;
	}
        
        setTime(timeConsumedMillis);
    }
    
    public float getProgress(){
        float progress = i/n * 100;
        return progress;
    }
    
    double f1(double x, double y1, double y2)
    {
            return ( 3*y1 + y2 );
    }

    double f2(double x, double y1, double y2)
    {
            return ( (-1)*y1 + 4*y2 );
    }
    
    private void setTime(long newTime) {
        time = newTime;
    }
    
    public long getTime() {
        return time;
    }
}
