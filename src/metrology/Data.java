/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metrology;

import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Кирилл
 */
public class Data {
    private ArrayList<Series> Series = new ArrayList();
    private double bad_test_1, bad_test_2, bad_test_3, bad_result;
    
    public void printSeries() {
        for (Series s : Series)
            s.print();
    }
    
    public void addSeries(String name, long test_1, long test_2, long test_3) {
        double result = Math.pow(test_1*test_2*test_3, 1.0/3.0);
        Series.add( new Series(name, test_1, test_2, test_3, result) );
    }
    
    public void addCurrent(String name, long test_1, long test_2, long test_3) {
        double result = Math.pow(test_1*test_2*test_3, 1.0/3.0)/bad_result;
        Series.add( new Series(name, test_1/bad_test_1, test_2/bad_test_2, test_3/bad_test_3, result) );
    }
    
    public void normalize() {
        double max_result = 0;
        
        int id_max=0;
        for( int i=0; i < Series.size(); i++ ) {
            if ( Series.get(i).name.equals("This PC") ) break;
            if( Series.get(i).result > max_result ) {
                max_result = Series.get(i).result;
                id_max = i;
            }
        }
        
        bad_test_1 = Series.get(id_max).test_1;
        bad_test_2 = Series.get(id_max).test_2;
        bad_test_3 = Series.get(id_max).test_3;
        bad_result = Series.get(id_max).result;
        for (Series s : Series) {
            s.test_1 /= bad_test_1;
            s.test_2 /= bad_test_2;
            s.test_3 /= bad_test_3;
            s.result /= bad_result;
        }
    }
    
    public void addAllToTable( DefaultTableModel model ) {
        int row_id=0;
        for (Series s : Series) {
            Object[] row = {s.name, s.test_1, s.test_2, s.test_3, s.result};
            if ( model.getRowCount() > row_id ) 
                model.removeRow(row_id);
            model.insertRow(row_id, row);
            row_id++;
        }
    }
    
    public void addCurrentToTable( DefaultTableModel model ) {
        int current = Series.size()-1;
        Object[] row = {Series.get(current).name, Series.get(current).test_1, Series.get(current).test_2, Series.get(current).test_3, Series.get(current).result};
        model.insertRow(model.getRowCount(), row);
    }
    
    public class Series {
        protected String name;
        protected double test_1,test_2,test_3, result;

        Series(String name, double test_1, double test_2, double test_3, double result) {
            this.name = name;
            this.test_1 = test_1;
            this.test_2 = test_2;
            this.test_3 = test_3;
            this.result = result;
        }
        
        public void print() {
            System.out.println( "Name: " + name + "\nT1: " + test_1 + "\nT2: " + test_2 + "\nT3: " + test_3 + "\nResult: " + result + "\n" );
        }
    }
}
