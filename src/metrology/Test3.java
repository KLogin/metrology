/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metrology;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Кирилл
 */
public class Test3 {
    private long time,start,timeConsumedMillis;

    public void copy(String source, String dest) {
        File folder_source = new File(source);
        File folder_dest = new File(dest);
        
        for(File f : folder_source.listFiles()) {
            try {
                start = System.currentTimeMillis();
                copy( f, new File( dest  + "\\" + f.getName() ) );
                timeConsumedMillis += System.currentTimeMillis() - start;
            } catch (IOException ex) {
                Logger.getLogger(Test3.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        setTime(timeConsumedMillis);
        
        for(File f : folder_dest.listFiles())
            f.delete();
    }
    
    public static void copy(File source, File dest) throws IOException {
        Files.copy(source.toPath(), dest.toPath());
    }
    
    private void setTime(long newTime) {
        time = newTime;
    }
    
    public long getTime() {
        return time;
    }
}
