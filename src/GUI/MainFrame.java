/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import metrology.Data;
import metrology.Test1;
import metrology.Test2;
import metrology.Test3;

/**
 *
 * @author Кирилл
 */
public class MainFrame extends javax.swing.JFrame{
    Data d = new Data();
    DefaultTableModel model;
    
    Test1 t1; 
    Test2 t2; 
    Test3 t3;
    String currentName;
    
    public MainFrame() {
        setLocationByPlatform(true);  
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        getContentPane().setBackground( new Color(200, 221, 242) );
  
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
         
        setSize( new Dimension( dim.width/2, dim.height/2 ) );
        setMinimumSize( new Dimension( dim.width/2, dim.height/2 ) );
        
        setTitle("Метрологическое тестирование");
        
        JScrollPane scrollPane;
        
        d.addSeries("PC 1", 1081, 711, 577);
        d.addSeries("PC 2", 1103, 636, 546);
        d.addSeries("PC 3", 510, 398, 535);
        
        Object[] columnNames = {"PC", "CPU", "GPU", "R/W", "Result"};
        Object[][] data = {};
        JTable table = new JTable( new DefaultTableModel( data, columnNames ) );
        model = (DefaultTableModel) table.getModel();
        
        d.addAllToTable(model);
        
        setLayout( new BorderLayout() );
        scrollPane = new JScrollPane(table);
        add(scrollPane);
        
        JPanel Buttons_bottom = new JPanel();
        JButton button_start = new JButton("Test");
        JButton button_normalize = new JButton("Normalize");
        Buttons_bottom.add(button_start);
        Buttons_bottom.add(button_normalize);
        add(Buttons_bottom, BorderLayout.PAGE_END);
        
        setVisible(true);
        
        button_start.addActionListener( new ActionListener () {
            @Override
            public void actionPerformed(ActionEvent e){
                t1.calculation();
                t2.draw();
                t3.copy("IN", "OUT");
                d.addSeries(currentName, t1.getTime(), t2.getTime(), t3.getTime());
                d.addCurrentToTable(model);
            }
        });
        
        button_normalize.addActionListener( new ActionListener () {
            @Override
            public void actionPerformed(ActionEvent e){
                d.normalize();
                d.addAllToTable(model);
                d.printSeries();
            }
        });
    }
    
    public void addCurrent(String currentName, Test1 t1, Test2 t2, Test3 t3) {
        this.t1 = t1;
        this.t2 = t2;
        this.t3 = t3;
        this.currentName = currentName;
    }
    
}

